package views;

import java.awt.Button;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import misc.event.FormInterface;

public class MainFrame extends Frame implements FormInterface {
	private MainFrame instance;
	private Button button1, button2, button3, button4, button5, button6;
	public MainFrame() {
		initComponents();
		addWindowListener(new WindowAdapter() {
		
			public void windowClosing(WindowEvent e) {
				dispose();
				System.exit(0);
			}
		});
			
			
		
	}
	@Override
	public void initComponents() {
		setLayout(null);
		setSize(390,200);
		setResizable(false);
		instance = this;
		
		button1= new Button("Componentes");
		button1.setBounds(80, 50, 100, 32);
		add(button1);
		
		button2= new Button("Graficos");
		button2.setBounds(210, 50, 100, 32);
		add(button2);
		
		button3= new Button("Imagenes");
		button3.setBounds(80, 100, 100, 32);
		add(button3);
		
		button4= new Button("Calculos");
		button4.setBounds(210, 100, 100, 32);
		add(button4);
		
		button5= new Button("Browser");
		button5.setBounds(80, 150, 100, 32);
		add(button5);
		
		button6= new Button("Menu");
		button6.setBounds(210, 150, 100, 32);
		add(button6);
		
		
	}

	@Override
	public void showForm() {
		setVisible(true);
		setLocationRelativeTo(null);
		toFront();
		
	}

	@Override
	public void clean() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showForm(boolean maximize) {
		// TODO Auto-generated method stub
		
	}

}
