package misc.event;

public interface FormInterface {
	void initComponents();
	void showForm();
	void clean();
	void showForm(boolean maximize);

}
